# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2010.
# Sergiu Bivol <sergiu@cip.md>, 2011, 2012, 2020, 2023, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2025-03-04 00:42+0000\n"
"PO-Revision-Date: 2024-02-24 15:53+0000\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sergiu Bivol"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sergiu@cip.md"

#: IdentitiesModel.cpp:27
#, kde-format
msgid "Select User"
msgstr "Alege utilizatorul"

#: IdentitiesModel.cpp:45
#, kde-format
msgctxt "%1 is the full user name, %2 is the user login name"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: main.cpp:48
#, kde-format
msgid "(c) 2009 Red Hat, Inc."
msgstr "(c) 2009 Red Hat, Inc."

#: main.cpp:49
#, kde-format
msgid "Lukáš Tinkl"
msgstr "Lukáš Tinkl"

#: main.cpp:49
#, kde-format
msgid "Maintainer"
msgstr "Responsabil"

#: main.cpp:50
#, kde-format
msgid "Jaroslav Reznik"
msgstr "Jaroslav Reznik"

#: main.cpp:50
#, kde-format
msgid "Former maintainer"
msgstr "Fostul responsabil"

#: policykitlistener.cpp:90
#, kde-format
msgid "Another client is already authenticating, please try again later."
msgstr "Un alt client se autentifică deja, reîncercați mai târziu."

#: policykitlistener.cpp:97
#, kde-format
msgctxt "Error response when polkit calls us with an empty list of identities"
msgid "No user to authenticate as. Please check your system configuration."
msgstr ""
"Nu se poate autentifica ca niciun utilizator. Verificați configurarea "
"sistemului."

#: qml/MobileQuickAuthDialog.qml:16 qml/MobileQuickAuthDialog.qml:108
#: qml/QuickAuthDialog.qml:16
#, kde-format
msgid "Authentication Required"
msgstr "Se cere autentificarea"

#: qml/MobileQuickAuthDialog.qml:57 qml/QuickAuthDialog.qml:68
#, kde-format
msgid "Authentication failure, please try again."
msgstr "Autentificare eșuată. Reîncercați."

#: qml/MobileQuickAuthDialog.qml:141
#, kde-format
msgid "User: %1"
msgstr "Utilizator: %1"

#: qml/MobileQuickAuthDialog.qml:146 qml/QuickAuthDialog.qml:177
#, kde-format
msgid "Switch…"
msgstr "Schimbă…"

#: qml/MobileQuickAuthDialog.qml:167 qml/QuickAuthDialog.qml:197
#, kde-format
msgid "Password…"
msgstr "Parolă…"

#: qml/MobileQuickAuthDialog.qml:175 qml/QuickAuthDialog.qml:206
#, kde-format
msgid "Details"
msgstr "Detalii"

#: qml/MobileQuickAuthDialog.qml:185 qml/QuickAuthDialog.qml:218
#, kde-format
msgid "OK"
msgstr "Bine"

#: qml/MobileQuickAuthDialog.qml:191 qml/QuickAuthDialog.qml:224
#, kde-format
msgid "Cancel"
msgstr "Renunță"

#: qml/MobileQuickAuthDialog.qml:201 qml/QuickAuthDialog.qml:236
#, kde-format
msgid "Action:"
msgstr "Acțiune:"

#: qml/MobileQuickAuthDialog.qml:205 qml/QuickAuthDialog.qml:240
#, kde-format
msgid "ID:"
msgstr "Identificator:"

#: qml/MobileQuickAuthDialog.qml:209 qml/QuickAuthDialog.qml:244
#, kde-format
msgid "Vendor:"
msgstr "Vânzător:"

#: qml/QuickAuthDialog.qml:171
#, kde-format
msgid "Authenticating as %1"
msgstr "Se autentifică ca %1"

#~ msgid "<null>"
#~ msgstr "<nul>"

#~ msgid "Action ID:"
#~ msgstr "Id. acțiune:"

#~ msgid "Password for root:"
#~ msgstr "Parola pentru root:"

#~ msgid "Password for %1:"
#~ msgstr "Parola pentru %1:"

#~ msgid "Password or swipe finger for root:"
#~ msgstr "Parola sau treceți degetul pentru root:"

#~ msgid "Password or swipe finger for %1:"
#~ msgstr "Parola sau treceți degetul pentru %1:"

#~ msgid "Password or swipe finger:"
#~ msgstr "Parola sau treceți degetul:"

#~ msgid ""
#~ "An application is attempting to perform an action that requires "
#~ "privileges. Authentication is required to perform this action."
#~ msgstr ""
#~ "O aplicație încearcă să efectueze o acțiune ce necesită privilegii. "
#~ "Pentru a efectua această acțiune este necesară autentificarea."

#~ msgctxt ""
#~ "%1 is the name of a detail about the current action provided by polkit"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "'Description' not provided"
#~ msgstr "„Descriere” nu a fost furnizată"

#~ msgid "Click to open %1"
#~ msgstr "Apăsați pentru a deschide %1"

#~ msgid "P&assword:"
#~ msgstr "P&arolă:"

#~ msgid "PolicyKit1 KDE Agent"
#~ msgstr "Agent-KDE PolicyKit1"

#~ msgid "Application:"
#~ msgstr "Aplicație:"

#~ msgid "Click to edit %1"
#~ msgstr "Apăsați pentru a edita %1"

#~ msgid "Switch to dialog"
#~ msgstr "Comută la dialog"

#~ msgid "Remember authorization"
#~ msgstr "Reține autorizarea"

#~ msgid "For this session only"
#~ msgstr "Numai pentru această sesiune"
